﻿using System;
using System.Collections.Generic;

namespace MazeSoln
{
    public class Cell
    {
        private int _colNum;
        private int _rowNum;
        private Boolean[] _walls = { true, true, true, true };
        private bool _visited;
        private int _totalColums;
        private int _totalRows;

        public Cell(int colNum, int rowNum)
        {
            this._totalColums = Constants.TotalWidth / Constants.CellWidth;
            this._totalRows = Constants.TotalHeight / Constants.CellWidth;
            this._colNum = colNum;
            this._rowNum = rowNum;
        }


        public void SetVisited(bool visited)
        {
            this._visited = visited;
        }

        public Boolean[] GetWalls()
        {
            return _walls;
        }

        public int GetColNum()
        {
            return _colNum;
        }

        public int GetRowNum()
        {
            return _rowNum;
        }

        public void SetWall0(bool wall)
        {
            _walls[0] = wall;
        }
        public void SetWall1(bool wall)
        {
            _walls[1] = wall;
        }
        public void SetWall2(bool wall)
        {
            _walls[2] = wall;
        }
        public void SetWall3(bool wall)
        {
            _walls[3] = wall;
        }

        public Cell GetRandomUnvisitedNeighbour(List<Cell> cells)
        {
            List<Cell> neighboursUnvisited = new List<Cell>();

            //top
            int topIndex = Index(this._colNum, this._rowNum - 1);

            if (topIndex >= 0 && !cells[topIndex]._visited)
            {
                neighboursUnvisited.Add(cells[topIndex]);
            }

            //right
            int rightIndex = Index(this._colNum + 1, this._rowNum);

            if(rightIndex >=0 && !cells[rightIndex]._visited)
            {
                neighboursUnvisited.Add(cells[rightIndex]);
            }

            //bottom
            int bottomIndex = Index(this._colNum, this._rowNum + 1);

            if (bottomIndex >= 0 && !cells[bottomIndex]._visited)
            {
                neighboursUnvisited.Add(cells[bottomIndex]);
            }

            //left
            int leftIndex = Index(this._colNum - 1, this._rowNum);

            if (leftIndex >= 0 && !cells[leftIndex]._visited)
            {
                neighboursUnvisited.Add(cells[leftIndex]);
            }
            if (neighboursUnvisited.Count > 0)
            {
                return neighboursUnvisited[new Random().Next(neighboursUnvisited.Count)];
            }

            return null;

        }

        public int Index(int i, int j)
        {
            if (i < 0 || j < 0 || i > this._totalColums - 1 || j > this._totalRows - 1)
            {
                return -1;
            }
            return i + (j - 1) * this._totalColums;

        }
    }
}
