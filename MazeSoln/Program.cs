﻿using System;
using System.Collections.Generic;

namespace MazeSoln
{
    class Program
    {
        private Print _print = new Print();
        private Cell _current;
        private List<Cell> _cells = new List<Cell>();
        private int _totalColumns;
        private int _totalRows;
        Stack<Cell> _backtracker = new Stack<Cell>();

        public Program()
        {
            this._totalColumns = Constants.TotalWidth / Constants.CellWidth;
            this._totalRows = Constants.TotalHeight / Constants.CellWidth;
        }

        public void SetUp()
        {
            for (int i = 0; i < _totalRows; i++)
            {
                for (int j = 0; j < _totalColumns; j++)
                {
                    Cell cell = new Cell(i, j);
                    _cells.Add(cell);
                }
            }
            _current = _cells[0];
        }

        public void Draw()
        {
            for (int i = 0; i < _cells.Count; i++)
            {
                Cell cell = _cells[i];
                _print.DisplayCell(cell.GetColNum() * Constants.CellWidth,
                    cell.GetRowNum() * Constants.CellWidth, cell.GetWalls());
            }
            _current.SetVisited(true);
            Cell next = _current.GetRandomUnvisitedNeighbour(_cells);

            if (next != null)
            {
                next.SetVisited(true);
                _backtracker.Push(_current);
                RemoveWalls(_current, next);
                _current = next;
            } else if(_backtracker.Count > 0)
            {
                _current = _backtracker.Pop();
            }
        }

        private void RemoveWalls(Cell a, Cell b)
        {
            int x = a.GetColNum() - b.GetColNum();
            if (x == 1)
            {
                a.SetWall3(false);
                b.SetWall1(false);
            }
            else if (x == -1)
            {
                a.SetWall1(false);
                b.SetWall3(false);
            }
            int y = a.GetRowNum() - b.GetRowNum();
            if (y == 1)
            {
                a.SetWall0(false);
                b.SetWall2(false);
            }
            else if (x == -1)
            {
                a.SetWall2(false);
                b.SetWall0(false);
            }
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.SetUp();
            program.Draw();

        }
    }
}
